package com.mycompany.spring.controller;

import com.mycompany.spring.model.DTO.AllDTO;
import com.mycompany.spring.model.DTO.DesireDTO;
import com.mycompany.spring.model.DTO.OfferDTO;
import com.mycompany.spring.model.DTO.SubjectDTO;
import com.mycompany.spring.model.Person;
import com.mycompany.spring.model.Subject;
import com.mycompany.spring.service.impl.PersonServiceImpl;
import com.mycompany.spring.service.impl.SubjectServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by artgr on 28.12.2016.
 */
@org.springframework.stereotype.Controller
public class SubjectController {
    @Autowired
    private SubjectServiceImpl subjectService;
    @Autowired
    private PersonServiceImpl personService;

    @RequestMapping(value = "/addsubject", method = RequestMethod.GET)
    public String events(Model model) {
        model.addAttribute("subjectForm", new Subject());
        model.addAttribute("subjects", subjectService.getAll());
        return "addsubject";
    }

    @RequestMapping(value = "/addsubject", method = RequestMethod.POST)
    public String addEvent(@ModelAttribute("subjectForm") Subject subjectForm, Model model) {
        if (subjectService.getByName(subjectForm.getName()) == null && subjectForm.getName() != "") {
            subjectService.addSubject(new Subject(subjectForm.getName()));
        }
        model.addAttribute("subjectForm", new Subject());
        model.addAttribute("subjects", subjectService.getAll());
        return "addsubject";
    }


    @RequestMapping(value = "/addsubject_delete")
    public String deleteEvent(@RequestParam("id") Integer id, Model model) {
        Subject subject = subjectService.getById(id);
        if ( subject != null) {
            Set<Person> desirePerson = subject.getStudentsDesire();
            Set<Person> offerPerson = subject.getStudentsOffer();
            for(Person person : desirePerson){
                person.getDesire().remove(subject);
                personService.editPerson(person);
            }
            for(Person person : offerPerson){
                person.getOffer().remove(subject);
                personService.editPerson(person);
            }
            subjectService.delete(id);
        }
        model.addAttribute("subjectForm", new Subject());
        model.addAttribute("subjects", subjectService.getAll());
        return "addsubject";
    }

    @RequestMapping(value = "/edit_desire", method = RequestMethod.GET)
    public String editDesireGET(Model model) {


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Person person = personService.findByMail(auth.getName());
        List<Subject> subjectsList = subjectService.getAll();

        List<SubjectDTO> all = new ArrayList<SubjectDTO>();
        List<SubjectDTO> desire = new ArrayList<SubjectDTO>();
        for (Subject subject : subjectsList) {
            if (!subject.getStudentsOffer().contains(person) && !subject.getStudentsDesire().contains(person)) {
                all.add(new SubjectDTO(subject.getName()));
            } else if (subject.getStudentsDesire().contains(person)) {
                desire.add(new SubjectDTO(subject.getName()));
            }
        }
        AllDTO allS = new AllDTO();
        DesireDTO desireS = new DesireDTO();
        allS.setSubjects(all);
        desireS.setSubjects(desire);
        model.addAttribute("allForm", allS);
        model.addAttribute("desireForm", desireS);
        return "editDesire";
    }

    @RequestMapping(value = "/edit_desire", method = RequestMethod.POST)
    public String editDesirePOST(@ModelAttribute("desireForm") DesireDTO desireDTO, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Person person = personService.findByMail(auth.getName());
        Set<Subject> desire = person.getDesire();
        Set<Subject> newDesire = new HashSet<>();
        System.out.println("All come2");
        for (SubjectDTO sub : desireDTO.getSubjects()) {
            if (sub != null) {


                if (sub.getName() != null) {
//                    if (!sub.getName().equals("NULL")) {

                    newDesire.add(new Subject(sub.getName()));
//                    }
                }

            }
        }
        if (newDesire.size() == 1) {
            try {

                for (Subject so : person.getDesire()) {
                    so.getStudentsDesire().remove(person);
                    person.getDesire().remove(so);
                    subjectService.editSubject(so);
                }
            } catch (Exception e) {
            }
            newDesire.clear();

        }
        for (Subject sub : newDesire) {
            if (!sub.getName().equals("NULL")) {
                Subject s = subjectService.getByName(sub.getName());
                if (s.getStudentsDesire() == null) {
                    s.setStudentsDesire(new HashSet<>());
                }
                if (!s.getStudentsDesire().contains(person)) {
                    s.getStudentsDesire().add(person);
                    subjectService.editSubject(s);
                    if (person.getDesire() == null) {
                        person.setDesire(new HashSet<>());
                    }
                    person.getDesire().add(s);
                } else {
                    sub = s;
                }
            }
        }
        List<Subject> alls = subjectService.getAll();
        for (Subject sb : alls) {
            boolean is = true;
            for (Subject sd : newDesire) {
                String name1 = sb.getName();
                String name2 = sd.getName();
                boolean tr = name1.equals(name2);
                if (tr) {
                    is = false;
                }
            }
            if (is) {
                try {
                    sb.getStudentsDesire().remove(person);
                    subjectService.editSubject(sb);
                } catch (Exception ex) {
                }
            }


        }
        personService.editPerson(person);
        return "redirect:profile";
    }

    @RequestMapping(value = "/edit_offer", method = RequestMethod.GET)
    public String editOfferGET(Model model) {


        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Person person = personService.findByMail(auth.getName());
        List<Subject> subjectsList = subjectService.getAll();

        List<SubjectDTO> all = new ArrayList<SubjectDTO>();
        List<SubjectDTO> offer = new ArrayList<SubjectDTO>();
        for (Subject subject : subjectsList) {
            if (!subject.getStudentsOffer().contains(person) && !subject.getStudentsDesire().contains(person)) {
                all.add(new SubjectDTO(subject.getName()));
            } else if (subject.getStudentsOffer().contains(person)) {
                offer.add(new SubjectDTO(subject.getName()));
            }
        }
        AllDTO allS = new AllDTO();
        OfferDTO offerS = new OfferDTO();
        allS.setSubjects(all);
        offerS.setSubjects(offer);
        model.addAttribute("allForm", allS);
        model.addAttribute("offerForm", offerS);
        return "editOffer";
    }



    @RequestMapping(value = "/edit_offer", method = RequestMethod.POST)
    public String editOfferPOST(@ModelAttribute("offerForm") OfferDTO offerDTO, Model model) {
        Person person = personService.findByMail(SecurityContextHolder.getContext().getAuthentication().getName());
        Set<Subject> desire = person.getOffer();
        Set<Subject> newOffer = new HashSet<>();
        System.out.println("All come3");
        for (SubjectDTO sub : offerDTO.getSubjects()) {
            if (sub != null) {
                if (sub.getName() != null) {
                    newOffer.add(new Subject(sub.getName()));
                }
            }
        }
        if (newOffer.size() == 1) {
            try {
                for (Subject so : person.getOffer()) {
                    so.getStudentsOffer().remove(person);
                    person.getOffer().remove(so);
                    subjectService.editSubject(so);
                }
            } catch (Exception e) {
            }
            newOffer.clear();
        }
        for (Subject sub : newOffer) {
            if (!sub.getName().equals("NULL")) {
                Subject s = subjectService.getByName(sub.getName());
                if (s.getStudentsOffer() == null) {
                    s.setStudentsOffer(new HashSet<>());
                }
                if (!s.getStudentsOffer().contains(person)) {
                    s.getStudentsOffer().add(person);
                    subjectService.editSubject(s);
                    if (person.getOffer() == null) {
                        person.setOffer(new HashSet<>());
                    }
                    person.getOffer().add(s);
                } else {
                    sub = s;
                }
            }
        }
        List<Subject> alls = subjectService.getAll();
        for (Subject sb : alls) {
            boolean is = true;
            for (Subject sd : newOffer) {
                if (sb.getName().equals(sd.getName())) {
                    is = false;
                }
            }
            if (is) {
                try {
                    sb.getStudentsOffer().remove(person);
                    subjectService.editSubject(sb);
                } catch (Exception ex) {
                }
            }
        }
        personService.editPerson(person);
        return "redirect:profile";
    }
}