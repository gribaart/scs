/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.controller;

import com.mycompany.spring.model.DTO.*;
import com.mycompany.spring.model.Event;
import com.mycompany.spring.model.Person;
import com.mycompany.spring.model.Role;
import com.mycompany.spring.model.Subject;
import com.mycompany.spring.service.impl.EventServiceImpl;
import com.mycompany.spring.service.impl.PersonServiceImpl;
import com.mycompany.spring.service.impl.RoleServiceImpl;
import com.mycompany.spring.service.impl.SubjectServiceImpl;
import com.mycompany.spring.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author artgr
 */
@Controller
public class PersonController {
    @Autowired
    private SubjectServiceImpl subjectService;

    @Autowired
    private EventServiceImpl eventService;

    @Autowired
    private PersonServiceImpl personService;

    @Autowired
    private RoleServiceImpl roleService;

    @Autowired
    private UserValidator userValidator;

    @RequestMapping(value = "/registration", method = RequestMethod.GET)
    public String registration(Model model) {
        model.addAttribute("userForm", new PersonDTO());
        return "registration";
    }

    @RequestMapping(value = "/registration", method = RequestMethod.POST)
    public String registration(@ModelAttribute("userForm") PersonDTO userForm, BindingResult bindingResult, Model model) {
        userValidator.validate(userForm, bindingResult);
        if (bindingResult.hasErrors()) {
            return "registration";
        }
        Person person = new Person(userForm.getUsername(), userForm.getPasswordConfirm());

        boolean first = false;


        Role user = roleService.getByRole("USER");
        Role admin = roleService.getByRole("ADMIN");

        if (user == null) {
            user = new Role("USER");
            admin = new Role("ADMIN");
            admin.setUsers(new HashSet<>());
            user.setUsers(new HashSet<>());
            Set<Person> persons = new HashSet<>();
            persons.add(person);
            admin.setUsers(persons);
            user.setUsers(persons);
            first = true;
        }
        admin.getUsers().add(person);
        user.getUsers().add(person);
        Set<Role> roles = new HashSet<>();
        roles.add(user);
        roles.add(admin);
        System.out.println(roles.size());
        for (Role r : roles) {
            System.out.println(r.getRole());
        }
        person.setRoles(roles);

        if (first) {
            roleService.addRole(admin);
            roleService.addRole(user);
        } else {
            roleService.editRole(admin);
            roleService.editRole(user);
        }

        for (Role r : person.getRoles()) {
            System.out.println(r.getRole());
            for (Person p : r.getUsers()) {
                System.out.println(p.getMail());
            }
            System.out.println();
        }
        System.out.println("OK");
        personService.addPerson(person);
        return "login";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
//    public String login(Model model, String error, String logout) {
    public String login(Model model, String error, String logout) {
        if (error != null)
            model.addAttribute("error", "Your username and password is invalid.");

        if (logout != null)
            model.addAttribute("message", "You have been logged out successfully.");

        return "login";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String loginPost(Model model, String error, String logout) {
        return "index";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.GET)
    public String editPersonInformation(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        model.addAttribute("person", personService.findByMail(auth.getName()));
        model.addAttribute("editForm", new PersonEditDataDTO());
        return "editForm";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public String editPersonInformationPost(@ModelAttribute("userForm") PersonEditDataDTO form, Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//        if (personService.findByMail(form.getMail()) != null && form.getMail() == auth.getName()) {
        System.out.println(form.getMail());
        System.out.println(auth.getName());
        //Why is false whan mail == name?
        boolean b = form.getMail() == auth.getName();
        System.out.println(b);

//        if ( (form.getMail() == auth.getName())) {
        Person oldPerson = personService.findByMail(auth.getName());
        oldPerson = personService.editPerson(new Person(oldPerson.getId(), form.getMail(), form.getPassword(), form.getName(), form.getSurname(), form.getCity(), form.getUniversity(), form.getFaculty(), form.getSpecialty(), form.getFacebook()));
        model.addAttribute("user", personService.findByMail(auth.getName()));
//        } else {
//            System.out.println("Email alrady use");
//        }


        List<Event> eventsList = eventService.getAll();
        Set<Event> updateList = new HashSet<Event>();
        for (Event ev : eventsList) {
            if (ev.getParticipants().contains(oldPerson)) {
                updateList.add(ev);
            }
        }
        model.addAttribute("events", updateList);


        return "profile";
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public String profil(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        Person person = personService.findByMail(auth.getName());
        List<Event> eventsList = eventService.getAll();
        List<Subject> subjectsList = subjectService.getAll();

        Set<Event> updateList = new HashSet<Event>();
        for (Event ev : eventsList) {
            if (ev.getParticipants().contains(person)) {
                updateList.add(ev);
            }
        }
        //model.addAttribute("subjects", subjects);
        model.addAttribute("events", updateList);
        model.addAttribute("user", person);

        List<SubjectDTO> all = new ArrayList<SubjectDTO>();
        List<SubjectDTO> desire = new ArrayList<SubjectDTO>();
        List<SubjectDTO> offer = new ArrayList<SubjectDTO>();
        for (Subject subject : subjectsList) {
            if (subject.getStudentsOffer().contains(person)) {
                offer.add(new SubjectDTO(subject.getName()));
            } else if (subject.getStudentsDesire().contains(person)) {
                desire.add(new SubjectDTO(subject.getName()));
            } else {
                all.add(new SubjectDTO(subject.getName()));
            }
        }


        AllDTO allS = new AllDTO();
        DesireDTO desireS = new DesireDTO();
        OfferDTO offerS = new OfferDTO();

        offerS.setSubjects(offer);
        allS.setSubjects(all);
        desireS.setSubjects(desire);


        model.addAttribute("allForm", allS);
        model.addAttribute("desireForm", desireS);
        model.addAttribute("offerForm", offerS);
        return "profile";
    }

}
