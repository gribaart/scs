/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * Class represent requirement for meeting
 *
 * @author bahlaliz
 */
@Entity
@Table
public class Meeting extends BaseEntity implements Serializable {
    private static final long serialVersionUID = 1L;


    @Column
    public boolean approve;

    //applicant - zadatel;answerer-odpovidaici
    @ManyToOne
    @JoinColumn(name = "applicant_ID")
    private Person applicant;

    @ManyToOne
    @JoinColumn(name = "answerer_ID")
    private Person answerer;


    public boolean isApprove() {
        return approve;
    }

    public void setApprove(boolean approve) {
        this.approve = approve;
    }

    public Person getApplicant() {
        return applicant;
    }

    public void setApplicant(Person applicant) {
        this.applicant = applicant;
    }

    public Person getAnswerer() {
        return answerer;
    }

    public void setAnswerer(Person answerer) {
        this.answerer = answerer;
    }


}
