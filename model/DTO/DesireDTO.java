package com.mycompany.spring.model.DTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artgr on 07.01.2017.
 */
public class DesireDTO {
    private List<SubjectDTO> subjects = new ArrayList<>();

    public DesireDTO() {
    }

    public List<SubjectDTO> getSubjects() {
        return subjects;
    }

    public void setSubjects(List<SubjectDTO> subjects) {
        this.subjects = subjects;
    }
}
