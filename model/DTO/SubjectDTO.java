package com.mycompany.spring.model.DTO;

/**
 * Created by artgr on 06.01.2017.
 */
public class SubjectDTO {
    private String name;

    public SubjectDTO() {
    }

    public SubjectDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
