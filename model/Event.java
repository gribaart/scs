/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.spring.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Class represent events
 * 
 * @author bahlaliz
 */
@Entity
@Table
public class Event extends BaseEntity implements Serializable{

    private static final long serialVersionUID = 1L;


    @Column
    private String name;

    @Column
    private String date;

    @Column
    private String description;

    @Column
    private String adres;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "person_event", joinColumns = @JoinColumn(name = "event_ID"), inverseJoinColumns = @JoinColumn(name = "person_ID"))
    private Set<Person> participants;

    public Event(String name, String date, String description, String adres) {
        this.name = name;
        this.date = date;
        this.description = description;
        this.adres = adres;
    }

    public Event() {
    }

    public void setAdres(String adres) {
        this.adres = adres;
    }

    public String getAdres() {
        return adres;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setAdress(String adres) {
        this.adres = adres;
    }

    public String getDate() {
        return date;
    }

    public String getDescription() {
        return description;
    }

    public String getAdress() {
        return adres;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Person> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<Person> participants) {
        this.participants = participants;
    }
    

}
